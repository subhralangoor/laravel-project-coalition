$(function() {
    try {
        $('.delete-record').on('click', function (e) {
            e.preventDefault();

            var target = $(this).attr('href');
            bootbox.confirm({
                title: "Delete record?",
                message: "Do you want to delete the record now? This cannot be undone.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result)
                    {
                        $('<form action="'+target+'" method="POST"><input type="hidden" name="_method" value="DELETE"></form>').appendTo('body').submit();
                    }
                }
            });
        });
    }
    catch (ex) {
        console.log(ex.message);
    }
});