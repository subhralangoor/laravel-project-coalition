<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Models\Project;
use App\Models\Task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $projects = new Project();
        $projects = $projects->pluck('project_name', 'id')->toArray();

        $task = new Task();
        $tasks = $task->with('project');
        if($request->has('project') && $request->input('project') > 0)
        {
            $tasks = $tasks->where('project_id', $request->input('project'));
        }
        $tasks = $tasks->orderBy('priority', 'asc')->get();

        return view('task.index', compact('projects', 'tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = new Project();
        $projects = $projects->pluck('project_name', 'id')->toArray();

        return view('task.create', compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'task_name' => 'required',
            'priority' => 'required',
            'project_id' => 'required'
        ]);

        $task = new Task();
        $task->task_name = $request->input('task_name');
        $task->priority = $request->input('priority');
        $task->project_id = $request->input('project_id');
        $task->save();

        return redirect()->route('tasks.create')
                        ->with('status', 'Task created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $projects = new Project();
        $projects = $projects->pluck('project_name', 'id')->toArray();

        return view('task.edit', compact('task', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $this->validate($request, [
            'task_name' => 'required',
            'priority' => 'required',
            'project_id' => 'required'
        ]);

        $task->task_name = $request->input('task_name');
        $task->priority = $request->input('priority');
        $task->project_id = $request->input('project_id');
        $task->save();

        return redirect()->route('tasks.edit', $task->id)
                        ->with('status', 'Task updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        Task::destroy($task->id);

        return redirect()->route('tasks.index')
                ->with('status', 'Task deleted successfully');
    }

    /**
     * Update the order of resources in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UpdateOrder(Request $request)
    {
        $i = 1;
        foreach ($request->order as $key => $value) {
            $task = Task::find($value);
            $task->priority = $i;
            $task->save();

            $i++;
        }
    }
}
