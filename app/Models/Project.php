<?php namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class Project extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}