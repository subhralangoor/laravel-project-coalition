<?php namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class Task extends Model
{
	/**
     * Get the project for the task.
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
}