@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Projects

                        <div class="pull-right">
                            <a href="{{ url('projects/create') }}" class="btn btn-sm btn-primary" role="button">Create Project</a>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="table-products" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Project Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($projects->count() > 0)
                                    @foreach ($projects as $key => $value)
                                        <tr>
                                            <td>{{ $value->id }}</td>
                                            <td>{{ $value->project_name }}</td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr id="no-record"><td colspan="5" align="center">No porjects to show</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent

    
@endsection