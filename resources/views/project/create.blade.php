@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => 'projects', 'method' => 'post', 'class' => '']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create Project
                            <div class="pull-right">
                                <a href="{{ url('projects') }}" class="btn btn-sm btn-default" role="button">Project Listing</a>
                            </div>
                        </div>

                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            
                            <div class="form-horizontal">
                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('project_name') ? ' has-error' : '' }}">
                                        {{ Form::label('project_name', 'Project Name *', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('project_name', null, ['class' => 'form-control', 'required']) }}

                                            @if ($errors->has('project_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('project_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <hr>
                            <div class="pull-right">
                                {{ Form::submit('Save', ['class' => 'btn btn-sm btn-primary']) }}
                                <a href="{{ url('projects') }}" class="btn btn-sm btn-default" role="button">Cancel</a>
                            </div>

                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent

@endsection