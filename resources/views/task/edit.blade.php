@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! Form::model($task, ['url' => 'tasks/'.$task->id, 'method' => 'put', 'class' => '']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Update Hotel
                            <div class="pull-right">
                                {{ Form::submit('Update', ['class' => 'btn btn-sm btn-primary']) }}
                                <a href="{{ url('tasks') }}" class="btn btn-sm btn-default" role="button">Cancel</a>
                            </div>
                        </div>

                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            
                            <div class="form-horizontal">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-sm{{ $errors->has('project_id') ? ' has-error' : '' }}">
                                            {{ Form::label('project_id', 'Project *', ['class' => 'col-md-4 control-label']) }}

                                            <div class="col-md-8">
                                                {{ Form::select('project_id', array('' => 'Select') + $projects, null, ['class' => 'form-control', 'required', 'autofocus']) }}

                                                @if ($errors->has('project_id'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('project_id') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-sm{{ $errors->has('task_name') ? ' has-error' : '' }}">
                                            {{ Form::label('task_name', 'Task Name *', ['class' => 'col-md-4 control-label']) }}

                                            <div class="col-md-8">
                                                {{ Form::text('task_name', null, ['class' => 'form-control', 'required']) }}

                                                @if ($errors->has('task_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('task_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-sm{{ $errors->has('priority') ? ' has-error' : '' }}">
                                            {{ Form::label('priority', 'Priority *', ['class' => 'col-md-4 control-label']) }}

                                            <div class="col-md-8">
                                                {{ Form::number('priority', null, ['min' => 1, 'class' => 'form-control', 'required']) }}

                                                @if ($errors->has('priority'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('priority') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <hr>
                            <div class="pull-right">
                                {{ Form::submit('Update', ['class' => 'btn btn-sm btn-primary']) }}
                                <a href="{{ url('tasks') }}" class="btn btn-sm btn-default" role="button">Cancel</a>
                            </div>

                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent

@endsection