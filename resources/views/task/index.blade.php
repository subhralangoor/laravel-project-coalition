@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tasks
                        <div class="pull-right">
                            <a href="{{ url('tasks/create') }}" class="btn btn-sm btn-primary" role="button">Create Task</a>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                {{ Form::select('project', array('' => 'All Projects') + $projects, null, ['id' => 'project', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="table-products" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Task Name</th>
                                        <th>Priority</th>
                                        <th>Project Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($tasks->count() > 0)
                                        @foreach ($tasks as $key => $value)
                                            <tr class="task-row" data-task-id="{{ $value->id }}">
                                                <td>{{ $value->id }}</td>
                                                <td>{{ $value->task_name }}</td>
                                                <td class="priority">{{ $value->priority }}</td>
                                                <td>{{ $value->project->project_name }}</td>
                                                <td>
                                                    <a href="{{ url('tasks/'.$value->id.'/edit') }}">Edit</a>
                                                    <a href="{{ url('tasks/'.$value->id) }}" class="delete-record">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr id="no-record"><td colspan="5" align="center">No tasks to show</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        $( "#project" ).change(function(e) {
            var value = $(this).val();
            var url = window.location.href.split("?project");
            window.location.href    = url[0]+'?project='+value;
        });

        $('tbody').sortable({
            update: function (e, ui) {
                //$('#content .update_message').show();

                // send reorder request
                var order = {};
                var i = 1;
                $( "tr.task-row" ).each(function( index ) {
                    order[index] = $( this ).data('task-id');
                    $( this ).find('.priority').text(i);

                    i++;
                });

                $.post('{{ url('tasks/update-order') }}', {
                        order: order
                    }
                );
            }
        });
    </script>
@endsection