## Installation instruction
1. Go to project directory
2. Run 'composer install'
3. Create a database
4. Change the database configuration in .env file
5. Run migration
```
php artisan migrate
```